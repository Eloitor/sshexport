# Makefile for the sshexport project
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS.adoc '/^[0-9]/s/:.*//p' | head -1)

MANDIR=/usr/share/man/man1
BINDIR=/usr/bin

DOCS    = README COPYING NEWS.adoc sshexport.adoc
SOURCES = sshexport Makefile $(DOCS)

.PHONY: pylint dist clean release

all: sshexport-$(VERS).tar.gz

install: sshexport.1
	cp sshexport $(BINDIR)
	gzip <sshexport.1 >$(MANDIR)/sshexport.1.gz

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

pylint:
	@pylint --score=n sshexport

reflow:
	black sshexport

sshexport-$(VERS).tar.gz: $(SOURCES) sshexport.1
	@ls $(SOURCES) sshexport.1 | sed s:^:sshexport-$(VERS)/: >MANIFEST
	@(cd ..; ln -s sshexport sshexport-$(VERS))
	(cd ..; tar -czvf sshexport/sshexport-$(VERS).tar.gz `cat sshexport/MANIFEST`)
	@(cd ..; rm sshexport-$(VERS))

dist: sshexport-$(VERS).tar.gz

clean:
	rm -f sshexport.1 sshexport.html
	rm -f *.1 MANIFEST index.html

release: sshexport-$(VERS).tar.gz sshexport.html
	shipper version=$(VERS) | sh -e -x

refresh: sshexport.html
	shipper -N -w version=$(VERS) | sh -e -x
